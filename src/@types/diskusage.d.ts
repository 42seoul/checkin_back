type DiskUsage = {
    available: number;
    free: number;
    total: number;
}